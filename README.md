# Recipe App API Proxy

NGINX Proxy App for our recipe app API


## Usage

### Evironments Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
